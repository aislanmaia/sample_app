require 'spec_helper'

describe "StaticPages" do

  subject { page }

  describe "Home page" do
    before { visit root_path }

    it { should have_selector('h1', text: 'Sample App')}    
    it { should have_selector('title', text: full_title(''))}    
    it { should_not have_selector('title', full_title('Home'))}

    after(:all) do
      User.delete_all
    end

    describe "for signed-in users" do      
      let!(:user) { FactoryGirl.create(:user) } # o metodo let é do tipo lazy(preguiçoso), ou seja,
                                               # a variável que ele instancia na verdade somente
                                               # passará a existir quando esta for referenciada.
      before do
        FactoryGirl.create(:micropost, user: user, content: "Lorem ipsum" )
        FactoryGirl.create(:micropost, user: user, content: "Dolor sit amet")
        sign_in user
        visit root_path
      end

      it "should render the user's feed" do
        user.feed.each do |item|
          page.should have_selector("li##{item.id}", text: item.content)
        end
      end

      describe "microposts pagination" do        
        before(:all) do           
          30.times { FactoryGirl.create(:micropost, user: user) } 
        end

        it { should have_selector('div.pagination') }        

        it "should list each micropost" do
          user.microposts.paginate(page: 1).each do |micropost|
            page.should have_selector('li', text: micropost.content)
          end
        end
      end  

      describe "sidebar count for many microposts" do
        it { should have_selector('span', text: 'microposts') }
      end    

      describe "sidebar count for just one micropost" do
        before do
          user.microposts.each do |micropost|
            if user.microposts.count > 1
              micropost.delete
            end
          end
        end

        it { should have_selector('span', text: 'micropost') }

      end

      describe "should don't display delete links for other microposts" do
        let(:other_user) { FactoryGirl.create(:user) }
        let(:micropost) { FactoryGirl.create(:micropost, user: other_user, content: 'Lorem Ipsum') }

        #before { visite user_path(other_user) }

        it { should_not have_link('delete', href: microposts_path(micropost)) }
      end

    end

  end
  
  describe "Help page" do
    before { visit help_path }

    it { should have_selector('h1', text: 'Help')}    
    it { should have_selector('title', text: full_title('Help'))}
  end
  
  describe "About page" do
    before { visit about_path }

    it { should have_selector('h1', text: 'About Us')}
    it { should have_selector('title', text: full_title("About Us")) }
  end 

  describe "Contact page" do
    before { visit contact_path }

    it{ should have_selector('title', text: full_title('Contact'))}
    it{ should have_selector('title', text: full_title("Contact"))}
  end

  
end
