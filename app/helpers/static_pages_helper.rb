module StaticPagesHelper
  
  def render_home_for_user_or_normal
    if signed_in?
      render 'shared/user_home'
    else
      render 'home_non_signed'
    end
  end

  def delete_link_for_micropost(item)
    if current_user?(item.user)
      link_to "delete", item, method: :delete, confirm: 'You sure?', title: item.content
    end
  end

end
