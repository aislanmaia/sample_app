module ApplicationHelper  

  def full_title(page_title)
    base_title = "Ruby on Rails Tutorial Sample App"
    if page_title.empty?
      base_title
    else
      "#{base_title} | #{page_title}"
    end
  end

  def remove_sensitive_error_messages_from_object(object)
    object.errors.full_messages.delete_if { |msg| msg =~ /digest/ }
  end

end
