module MicropostsHelper

  def wrap(content)
    sanitize(raw(content.split.map{ |word| wrap_long_string(word) }.join(' ') ))
  end

  def wrap_long_string(word, max_width = 30)
    zero_width_space = "&#8203;" # caracter não-imprimível que delimita uma string
                                # geralmente com uma quebra de linha

    regex = /.{1,#{max_width}}/ # expressão regular que procura por uma string com
                                # tamanho de 1 à max_width

    # Quando uma palavra (string) for maior que o tamanho máximo (max_width)
    # o método scan irá verificar na palavra em que caractere o tamanho máximo
    # é alcançado, atribuindo à ele através do método join o caracter delimitador
    # zero_width_space e retornando essa palavra modificada.
    word.length < max_width ? word : word.scan(regex).join(zero_width_space)

  end

  def count_char(content_length = 0, limit = 140)
    count = limit - content_length
    "#{count} character(s) left"
  end

end