# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/
jQuery -> 

  $("#micropost_content").keyup (e) ->    
    count = count_char()
    if count >= 0
      $("#count_char").html(count + " character(s) left.")
      $("#micropost_content").parent('div').removeClass('field_with_errors')
      $("#count_char").removeClass('alert-error')
    else
      $("#count_char").html("Limit of character(s) exceded.") 
      $("#count_char").addClass('alert-error')

  count_char = ->
    count = $("#micropost_content").val().length
    return count = 140 - count
        