class UsersController < ApplicationController
  before_filter :signed_in_user, only: [:index, :edit, :update, :destroy]
  before_filter :correct_user,   only: [:edit, :update]
  before_filter :admin_user,     only: [:destroy]  
  #before_filter {redirect_to root_path, notice: 'Not permitted.' if signed_in? && request_new_or_create?}
  before_filter :invalid_access, only: [:new, :create], if: :signed_in?
  before_filter :store_location

  def index
    @users = User.paginate(page: params[:page])
  end

  def new
  	@user = User.new
  end

  def show
  	@user = User.find(params[:id])
    store_location
    @microposts = @user.microposts.paginate(page: params[:page])
  end

  def create
  	@user = User.new(params[:user])
  	if @user.save
      sign_in @user # Logando o usuário após o cadastro
      flash[:success] = "Welcome to the Sample App!"
      redirect_to @user
  	else
  	  render 'new'	
  	end
  end

  def edit
  end

  def update    
    if @user.update_attributes(params[:user])
      flash[:success] = "Profile updated"
      sign_in @user     # Relogamos o usuário, pois quando salvamos novamente o usuário, o remember_token é resetado
                        # invalidando a sessão do usuário. Essa invalidação de sessão é um ótimo recurso, pois signi-
                        # fica que qualquer sessões hackeadas automaticamente expirarão quando a info do usuário for
                        # atualizada.
      redirect_to @user
    else
      render 'edit'
    end
  end

  def destroy
    user = User.find(params[:id])
    unless user.admin?
      user.destroy 
      flash[:success] = "User destroyed."
      redirect_to users_path
    else
      redirect_to users_path, notice: "You can't destroy a admin user."
    end
  end

  private
    
    def correct_user
      @user = User.find(params[:id])
      redirect_to root_path unless current_user?(@user)    
    end

    def admin_user
      redirect_to root_path unless current_user.admin?              
    end

    def invalid_access
      redirect_to root_path, notice: 'Not permitted.'
    end

end
