class MicropostsController < ApplicationController
  before_filter :signed_in_user, only: [:create, :destroy]
  before_filter :correct_user,   only: :destroy

  def index
  end

  def create  
    @micropost = current_user.microposts.build(params[:micropost])  
    @feed_items = @micropost.user.microposts.paginate(page: params[:page])
    respond_to do |format|
      if @micropost.save    
        flash[:success] = "Micropost created."
        format.js
      else
         format.js { @micropost }        
         @feed_items = []      
        # render 'static_pages/home'
      end          
    end


    
  end

  def destroy
    @micropost.destroy
    redirect_back_or root_path
  end

  private

    def correct_user
      @micropost = current_user.microposts.find_by_id(params[:id])
      redirect_to root_path if @micropost.nil?
    end 
  
  
end