class SessionsController < ApplicationController
  before_filter :has_signed?, only: [:new]

  def new
  end

  def create
  	user = User.find_by_email(params[:session][:email])  
  	if user && user.authenticate(params[:session][:password])
      sign_in user
      redirect_back_or user      
  	else
      flash.now[:error] = 'Invalid email/password combination' # flash.now é especialmente para mostrar mensagens
                                                               # em paginas renderizadas. Diferente do conteudo de
                                                               # flash, seu conteúdo desaparece assim que haja
                                                               # uma nova requisição.                                                               # 
      render 'new'
  	end
  end

  def destroy
    sign_out 
    redirect_to root_path
  end

  private

    def has_signed?      
      redirect_to session[:return_to], notice: 'You are already sign in.' if signed_in?
    end    

end
