class ApplicationController < ActionController::Base
  protect_from_forgery
  include SessionsHelper	

  def current_url
    url_for only_path: false, overwrite_params: {}
  end

  def request_new_or_create?
    request.path_parameters[:action] == "new" || request.path_parameters[:action] == "create"
  end

end